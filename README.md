# generator-polymer-init-polytempl-project-template [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url] [![Coverage percentage][coveralls-image]][coveralls-url]
> Polytempl project template

## Installation

First, install [Yeoman](http://yeoman.io) and generator-polymer-init-polytempl-project-template using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-polymer-init-polytempl-project-template
```

Then generate your new project:

```bash
yo polymer-init-polytempl-project-template
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

Apache-2.0 © [Uladzimir Urbanovich]()


[npm-image]: https://badge.fury.io/js/generator-polymer-init-polytempl-project-template.svg
[npm-url]: https://npmjs.org/package/generator-polymer-init-polytempl-project-template
[travis-image]: https://travis-ci.org//generator-polymer-init-polytempl-project-template.svg?branch=master
[travis-url]: https://travis-ci.org//generator-polymer-init-polytempl-project-template
[daviddm-image]: https://david-dm.org//generator-polymer-init-polytempl-project-template.svg?theme=shields.io
[daviddm-url]: https://david-dm.org//generator-polymer-init-polytempl-project-template
[coveralls-image]: https://coveralls.io/repos//generator-polymer-init-polytempl-project-template/badge.svg
[coveralls-url]: https://coveralls.io/r//generator-polymer-init-polytempl-project-template
