/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

'use strict';

const path = require('path');
const gulp = require('gulp');
const gulpif = require('gulp-if');
const compileHtmlTags = require('gulp-compile-html-tags');
const sass = require('gulp-sass');
const babel = require("gulp-babel");
const nodemon = require('gulp-nodemon');
const builder = require('polytempl');
const through = require('through2').obj;


global.config = {
  polymerJsonPath: path.join(process.cwd(), 'polymer.json'),
  build: {
    rootDirectory: 'build',
    bundledDirectory: 'bundled',
    unbundledDirectory: 'unbundled',
    bundleType: 'both'
  },
};


const clean = require('./gulp-tasks/clean.js');
const images = require('./gulp-tasks/images.js');
const project = require('./gulp-tasks/project.js');


function source() {
    return project.splitSource()
    // Add your own build tasks here!
        .pipe(gulpif('**/*.{png,gif,jpg,svg}', images.minify()))
        .pipe(gulpif('**/*.html', builder([process.cwd() + '/app/bower_components']) ))
        .pipe(gulpif('**/*.html', compileHtmlTags('style', function (tag, data) {
            return data.pipe(sass())
        }) ))
        .pipe(gulpif(['**/*.html', '!**/index.html'], compileHtmlTags('script', function (tag, data) {
            return data.pipe(babel());
        }) ))
        .pipe(gulpif('**/*', through(function(file, enc, cb) {
            file.base += '/app';
            if (file.path.indexOf('assets') >= 0) file.base += '/assets';
            cb(null, file)
        }) ))
        .pipe(project.rejoin()); // Call rejoin when you're finished
}


function dependencies() {
  return project.splitDependencies()
      .pipe(gulpif('**/*', through(function(file, enc, cb) {
        file.base += '/app';
        cb(null, file)
      }) ))
    .pipe(project.rejoin());
}

gulp.task('start', function () {
  nodemon({
    script: 'server.js'
  })
});

gulp.task('watch', function () {
  gulp.watch(['./app/elements/**/*','./app/index.html','./app/assets/**/*.*'], gulp.series('default'));
});


gulp.task('build', gulp.series([
  clean([global.config.build.rootDirectory]),
  project.merge(source, dependencies)
]));


gulp.task('default', gulp.series(['build']));
