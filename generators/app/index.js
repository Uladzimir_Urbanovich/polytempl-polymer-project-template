'use strict';
var yeoman = require('yeoman-generator');
var chalk = require('chalk');
var yosay = require('yosay');

module.exports = yeoman.Base.extend({
  prompting: function () {
    // Have Yeoman greet the user.
    this.log(yosay(
      'Welcome to the smashing ' + chalk.red('generator-polymer-init-polytempl-project-template') + ' generator!'
    ));

    var prompts = [];

    return this.prompt(prompts).then(function (props) {
      // To access props later use this.props.someAnswer;
      this.props = props;
    }.bind(this));
  },

  writing: function () {

    this.fs.copy(
        `${this.templatePath()}/**/*`,
        this.destinationPath()
    );
    this.fs.copy(
        `${this.templatePath()}/.*`,
        this.destinationPath()
    );

  },

  install: function () {
    this.installDependencies();
  }
});
